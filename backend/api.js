const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const expressOasGenerator = require('express-oas-generator');
//const swaggerUi = require('swagger-ui-express');
//const swaggerDocument = require('./swagger.json')
const urlencodedParser = bodyParser.urlencoded({ extended: true })
const cors = require ("cors")
const app = express();
expressOasGenerator.init(app, {});
//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
var originsWhitelist = [
        'http://localhost:4200',      //this is my front-end url for development
];
var corsOptions = {
        origin: function (origin, callback) {
                var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
                callback(null, isWhitelisted);
        },
        credentials: true
}
//here is the magic
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//  Connect mongoose to DB
let myDB = 'mongodb://localhost:27017/grouplib'
mongoose.connect(myDB, { useNewUrlParser: true }, function (err, db) {
        if (err) throw err
        console.log("It's connected!")
});

let usersSchema = mongoose.Schema({
        name: String
})

var Users = mongoose.model('users', usersSchema)

app.post('/user', (req, res) => {
       let user = new Users();
       user.name = req.body.name
        user.save( (err)=> {
                if (err) {
                        res.send(err)
                } else {
                        res.send({ message: 'Bravo' })
                }
        })
})



 //Collect data from DB and send to front

app.get('/users', (req, res) => {
        let users = new Users()
        Users.find((err, users) => {
                if (err) {
                res.send(err)
                } else {
                //res.json(users)
                res.send(users)
                console.log(users)
                }
        })
})

/*
//Making group
app.get('/binomes', (req, res) => {
        Users.find((err, users) => {
                if (err) {
                        res.send(err)
                } else {
                        // res.send(users)
                        // console.log('lister all' + users)
                 lookGroup(users, 2)
                 function lookGroup(users, nbCpl) {
                                let couple = 0;                       // compte de personnes dans group, nous avons besoin de 2 personnes dans un groupe
                                let myGroup = '';                  // string going to stocke 2 names d'un couple (2noms)
                                let nameGood = '';                // string stocke le nom sélectionné au hasard
                                let LongTab = users.length;  // stocke la longueur de ma table à l'origine de la requette
                                let coupleTb = [];

                                for (let i = 0; i < LongTab; i++) {
                                       let myUsers = users;  //clone la table users pour ne pas la modifiier on va se servir de la new table pour travailler

                                        // calcule un nb aléatoire à partir de la longueur du tableau users -->qui diminue au fur et à mesure
                                        nbAleatoire = Math.floor(Math.random() * users.length); //console.log('nb aleatoire  => ' + nbAleatoire)

                                        // sélectionne dans nameGood un enregistrement au hasard et le supprime dans le tableau users (qui diminu)
                                        nameGood = users.splice(nbAleatoire, 1)[0]
                                        //console.log('ici  nameGood ===== ' + nameGood.name)
                                        couple++;
                                        myGroup += nameGood.name + ' ';
                                       // couple == nbCpl ? (console.log('Couple ici = ' + myGroup), coupleTb.push(myGroup), couple = 0, myGroup = '') : '';
                                        couple == nbCpl ? (coupleTb.push(myGroup), couple = 0, myGroup = '') : '';
                                }
                               
                                res.send(coupleTb);
                                console.log(coupleTb);
                               // return coupleTb;

                        }

                }
        })
})
*/



app.get('/binomes', (req, res) => {
        Users.find((err, users) => {
                if (err) {
                        res.send(err)
                } else {
                        
                        let total = (users.length%2==0 ? users.length/2 : (users.length +1)/2);
                        let listBinome = [];

                        for (let i = 0; i < total; i++) {
                                
                                let group = [];
                                let randomIndex = Math.floor(Math.random() * (users.length));
                                group.push(users[randomIndex])
                                users.splice(randomIndex, 1)

                                randomIndex = Math.floor(Math.random() * (users.length));
                                group.push(users[randomIndex])
                                users.splice(randomIndex, 1)

                                listBinome.push(group)
                        }
                       

                        res.send(listBinome)
                        

                }
        })
})


 app.listen(4002, () => console.log('Express server is running port 4002'));